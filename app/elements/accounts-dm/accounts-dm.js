
  let items = [];
  
  const shuffleArray = (items) => {
    const shuffled = [ ...items ].sort(() => 0.5 - Math.random());
    const length = items.length;
    return shuffled.slice(0, length);
  };
  
  export async function getAccounts(id) {

    const { host, accounts} = window.AppConfig;

    const endpoint = `${host}/${accounts}/${id}`;
    const result = await fetch(endpoint).then((response) => response.json());
    if(result.length !== 0) {
      items = shuffleArray(result);
    }
    return items;
  }

  export async function createAccount(body) {
    const { host, accounts} = window.AppConfig;
    const endpoint = `${host}/${accounts}`;
    const result = await fetch(endpoint,
        {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(body),
          }
    );
    return result.json();
  }

  export async function updateAccount(body) {
    const { host, accounts} = window.AppConfig;
    const endpoint = `${host}/${accounts}/${body.id_account}`;
    const result = await fetch(endpoint,
        {
            method: 'PUT',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(body),
          }
    );
    return result.json();
  }

  export function cleanUp() {
    items = [];
  }

  export function genCreditCard() {
      return '0011' + genNumber(12);
  }

  function genNumber(len) {
    var credit = '';
    var abc = ["0","1","2","3","4","5","6","7","8","9"];
    var number = len;
    var numberRandom = 3;
    for(var i = 0; i<number; i++){
      numberRandom = parseInt(Math.random()*abc.length);
        credit += abc[numberRandom];
      }
      return credit;
  }

  export function genAmount() {
    return Math.round(Math.random() * (5000 - 500) + 500);
  }

  export function genAccountName() {
    let cardsNames = [
      'Cuenta Ahorro',
      'Cuenta Latam Pass',
      'Cuenta Sueldo',
      'Cuenta Ganadora',
      'Cuenta Facil'
    ]
    return cardsNames[Math.round(Math.random() * (4 - 0) + 0)];
  }


