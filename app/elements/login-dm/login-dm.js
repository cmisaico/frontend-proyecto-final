
  let items = [];


  export async function login(body) {
    const { host, login} = window.AppConfig;
    const endpoint = `${host}/${login}`;
    const result = await fetch(endpoint,
        {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(body),
          }
    );
    return result.json();
  }

