  let items = [];

  const shuffleArray = (items) => {
    const shuffled = [ ...items ].sort(() => 0.5 - Math.random());
    const length = items.length;
    return shuffled.slice(0, length);
  };
  
  export async function getUsers() {
    const { host, users} = window.AppConfig;
    const endpoint = `${host}/${users}`;
    const result = await fetch(endpoint).then((response) => response.json());
    if(result.length !== 0){
        items = shuffleArray(result);
    }
    return items;
  }
  
  export async function createUser(body) {
    const { host, users} = window.AppConfig;
    const endpoint = `${host}/${users}`;
    const result = await fetch(endpoint,
        {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(body),
          }
    );
    return result.json();
  }

  export async function deleteUser(id) {
    const { host, users} = window.AppConfig;
    const endpoint = `${host}/${users}/${id}`;
    const result = await fetch(endpoint,
        {
            method: 'DELETE',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
              'Content-Type': 'application/json',
            }
          }
    );
    return result.json();
  }

  export function cleanUp() {
    fetched = false;
    items = [];
  }
  

  export function generatePassword() {
    var password = '';
    var abc = ["a","b","c","0","1","2"];
	var number = 6;
	var numberRandom = 3;
	for(var i = 0; i<number; i++){
		numberRandom = parseInt(Math.random()*abc.length);
        password += abc[numberRandom];
    }
    return password;
  }