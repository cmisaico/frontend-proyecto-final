let fetched = false;
let items = [];

const shuffleArray = (items) => {
  const shuffled = [ ...items ].sort(() => 0.5 - Math.random());
  const length = items.length;
  return shuffled.slice(0, length);
};

export async function getMovements(id) {
  const { host, movements} = window.AppConfig;
  const endpoint = `${host}/${movements}/${id}`;
  const result = await fetch(endpoint).then((response) => response.json());
  if(result.length !== 0){
      items = shuffleArray(result);
  }
  return items;
}

export async function createMovement(body) {
  const { host, movements} = window.AppConfig;
  const endpoint = `${host}/${movements}`;
  const result = await fetch(endpoint,
      {
          method: 'POST',
          mode: 'cors',
          cache: 'no-cache',
          credentials: 'same-origin',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(body),
        }
  );
  return result.json();
}

export function cleanUp() {
  items = [];
}
