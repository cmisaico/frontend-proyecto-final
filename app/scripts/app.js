(function() {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'login': '/',
      'dashboard': '/dashboard',
      'account-detail': '/account/',
      'transfer': '/transfer',
      'users-admin': '/users-admin',
      'help': '/help'
    }
  });
}());
