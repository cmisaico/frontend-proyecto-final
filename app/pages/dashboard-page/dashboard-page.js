import { CellsPage } from '@cells/cells-page';
import { CellsI18nMixin as i18n } from '@cells-components/cells-i18n-mixin/cells-i18n-mixin.js';
import { html, css } from 'lit-element';
import { spread } from '@open-wc/lit-helpers';

//import '@bbva-web-components/bbva-list-movement';
import '@bbva-web-components/bbva-list-card';
import '@bbva-web-components/bbva-help-modal';

//import { getMovements } from '../../elements/movements-dm/movements-dm.js';
import { getAccounts } from '../../elements/accounts-dm/accounts-dm.js';
import { normalizeUriString } from '../../elements/utils/text.js';

class DashboardPage extends i18n(CellsPage) {
  static get is() {
    return 'dashboard-page';
  }

  constructor() {
    super();
    this.movements = [];
    this.accounts = [];
  }

  static get properties() {
    return {
      userName: { type: String },
      id: { type: String },
      movements: { type: Array },
      accounts: {type: Array}
    };
  }

  firstUpdated(changedProps) {
    super.firstUpdated(changedProps);

    this._logoutModal = this.shadowRoot.querySelector('#logoutModal');
  }

  onPageEnter() {
    this.subscribe('user_name', (userName) => this.userName = userName);
    this.subscribe('user_id', (userId) => this.id = userId);
    if (!this.accounts.length) {
      getAccounts(this.id).then((accounts) => {
        this.accounts = accounts;
      });
    }
  }

  onPageLeave() {

  }

  handleAccountClick(account) {
    this.publish('account_title', account.name);
    this.publish('id_account', account.id_account);
    this.publish('id_user', account.id_user);
    this.publish('name', account.name);
    this.publish('amount', account.amount);
    this.publish('number', account.number);
    this.navigate('account-detail');
  }

  get movementList() {
    if (!this.movements.length) {
      return null;
    }

    return this.movements.map((movement) => {
      const movementProperties = this.buildMovementProperties(movement);

      return html`
        <bbva-list-movement
          ...="${spread(movementProperties)}">
        </bbva-list-movement>
      `;
    });
  }

  get accountList() {
    if (!this.accounts.length) {
      return html`
        <div></div>
      `;
    }
    return this.accounts.map((account) => {
      const accountProperties = this.buildAccountProperties(account);
      return html`
        <bbva-list-card
          ...="${spread(accountProperties)}">
        </bbva-list-card>
      `;
    });
  }


  buildAccountProperties(account) {
    const { id_account, name, number, amount } = account;
    return {
      ...(name && { 'card-title': name }),
      ...(number && { 'num-product': number }),
      ...(amount && { 'amount': amount}),
      '@click': () => this.handleAccountClick(account),
      'class': 'bbva-global-semidivider',
      'aria-label': 'Ver detalle del pago con tarjeta',
      'language': 'es',
    };
  }



  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__header">
          <bbva-header-main
            icon-left1="coronita:on"
            accessibility-text-icon-left1="Cerrar Sesión"
            @header-icon-left1-click=${() => this._logoutModal.open()}
            text=${this.t('dashboard-page.header', '', { name: this.userName })}>
          </bbva-header-main>
        </div>

        <div slot="app__main">
          <div slot="app__section" class='container'>
            <label class='account-style-text'>Hola, ${this.userName}</label>
          </div>
          <div slot="app__section">
            ${this.accountList ? html`${this.accountList}` : html`<cells-skeleton-loading-page visible></cells-skeleton-loading-page>`}
          </div>
          <bbva-help-modal
            id="logoutModal" 
            header-icon="coronita:info" 
            header-text=${this.t('dashboard-page.logout-modal.header')}
            button-text=${this.t('dashboard-page.logout-modal.button')}
            @help-modal-footer-button-click=${() => this.navigate("login")}>
            <div slot="slot-content">
              <span>${this.t('dashboard-page.logout-modal.slot')}</span>
            </div>
          </bbva-help-modal>
        </div>
     </cells-template-paper-drawer-panel>`;
  }

  static get styles() {
    return css`
      bbva-header-main {
        --bbva-header-main-bg-color: #002171;
      }
      cells-skeleton-loading-page {
        margin-top: 11.5em;
      }
      .container {
        padding-top: 2em;
        display: flex;
        flex-direction: column;
        align-items: center;
      }
      cells-template-paper-drawer-panel {
        background-color: #5472d3;
      }
      .account-style-text {
        font-size: 22px;
        font-weight: bold;
      }
      bbva-list-movement {
        border-bottom: 1px solid #ccc;
      }
    `;
  }
}

window.customElements.define(DashboardPage.is, DashboardPage);