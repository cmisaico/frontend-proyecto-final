import { CellsPage } from '@cells/cells-page';
import { CellsI18nMixin as i18n } from '@cells-components/cells-i18n-mixin/cells-i18n-mixin.js';
import { html, css } from 'lit-element';
import { spread } from '@open-wc/lit-helpers';

import '@bbva-web-components/bbva-list-card';
import '@bbva-web-components/bbva-help-modal';
import '@bbva-web-components/bbva-list-contact';
import { genCreditCard, genAmount, genAccountName, createAccount } from '../../elements/accounts-dm/accounts-dm.js';
import { getUsers, generatePassword, createUser, deleteUser } from '../../elements/users-dm/users-dm.js';
import { normalizeUriString } from '../../elements/utils/text.js';

class UsersAdminPage extends i18n(CellsPage) {
  static get is() {
    return 'users-admin-page';
  }

  static get properties() {
    return {
      usersAdminTitle: { type: String },
      users: { type: Array },
      statusNew: { type: Boolean },
      statusSave: { type: Boolean },
    };
  }

  constructor() {
    super();
    this.users = [];
    this.statusSave = false;
    this.statusNew = true;
  }

  onPageEnter() {
    this.subscribe('user_admin_title', (usersAdminTitle) => this.usersAdminTitle = usersAdminTitle);
    if (!this.users.length) {
      getUsers().then((users) => {
        this.users = users;
      });
    }
  }

  deleteUserAccount(id) {
    console.log('funciona : ' + id);
    deleteUser(id).then(
      (result) => {
        getUsers().then((users)=>{
          this.users = users;
          this.shadowRoot.querySelector('#user-name').value = '';
          this.shadowRoot.querySelector('#user-last-name').value = '';
          this.shadowRoot.querySelector('#user-email').value = '';
          this.shadowRoot.querySelector('#user-dni').value = '';
          this.render();
        });
      }
    );
  }

  onPageLeave() {
    this.users = [];
  }



  get usersList() {
    if (!this.users.length) {
      return html`<li></li>
      `;
    }
      return this.users.map((user) => {
        return html`
        <li>
          <bbva-list-contact>
            ${user.first_name} ${user.last_name}
            <span slot="info">${user.dni}</span>
            
            <span slot="extra"><a class='link-delete' @click=${() => this.deleteUserAccount(user.id_user)}>Eliminar</a></span>
          </bbva-list-contact>
        </li>
        `;
      });

  }


  createUserAccount(){
    let user = {
      first_name: this.shadowRoot.querySelector('#user-name').value,
      last_name: this.shadowRoot.querySelector('#user-last-name').value,
      email: this.shadowRoot.querySelector('#user-email').value,
      password: generatePassword(),
      role: "USER",
      dni: this.shadowRoot.querySelector('#user-dni').value
    }
    createUser(user).then(
      (result) => {
        let id = result.id_user;
        let account = {
          idUser: id,
          name: genAccountName(),
          amount: genAmount(),
          number: genCreditCard()
        }
        createAccount(account).then(()=>{
          account = {
          idUser: id,
          name: genAccountName(),
          amount: genAmount(),
          number: genCreditCard()
          }
          createAccount(account);
          });
        getUsers().then((users)=>{
          this.users = users;
          this.shadowRoot.querySelector('#user-name').value = '';
          this.shadowRoot.querySelector('#user-last-name').value = '';
          this.shadowRoot.querySelector('#user-email').value = '';
          this.shadowRoot.querySelector('#user-dni').value = '';
          this.render();
        });
      }
    );
  }

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__header">
          <bbva-header-main
            icon-left1="coronita:return-12"
            accessibility-text-icon-left1="Volver"
            text=${this.usersAdminTitle}
            @header-icon-left1-click=${() => window.history.back()}>
          </bbva-header-main>
        </div>

        <div slot="app__main">
        <div  class="container">
          <bbva-form-field
            id="user-dni"
            auto-validate
            validate-on-blur 
            label=${this.t('users-page.dni')}
            required>
          </bbva-form-field>

          <bbva-form-field
            id="user-name"
            auto-validate
            validate-on-blur 
            label=${this.t('users-page.name')}
            required>
          </bbva-form-field>

          <bbva-form-field
            id="user-last-name"
            auto-validate
            validate-on-blur 
            label=${this.t('users-page.last_name')}
            required>
          </bbva-form-field>

          <bbva-form-field
            id="user-email"
            auto-validate
            validate-on-blur 
            label=${this.t('users-page.email')}
            required>
          </bbva-form-field>


          <bbva-button-default 
          @click=${this.createUserAccount}>
            ${this.t('users-page.save')}
        </bbva-button-default>

        </div>

          <span class="content style-scope bbva-list-contact">
          <ul>
          ${this.usersList ? html`${this.usersList}` : html`<cells-skeleton-loading-page visible></cells-skeleton-loading-page>`}
          </ul>
          </span>

          <bbva-help-modal
            id="logoutModal" 
            header-icon="coronita:info" 
            header-text=${this.t('dashboard-page.logout-modal.header')}
            button-text=${this.t('dashboard-page.logout-modal.button')}
            @help-modal-footer-button-click=${() => window.cells.logout()}>
            <div slot="slot-content">
              <span>${this.t('dashboard-page.logout-modal.slot')}</span>
            </div>
          </bbva-help-modal>
        </div>
     </cells-template-paper-drawer-panel>`;
  }

  static get styles() {
    return css`
      .link-delete {
        color: red;
      }
      ul {
        list-style: none;
      }
      ul li {
        margin-left: -2.6em;
        border-top: 1px solid #ccc;
      }
      .container {
        display: flex;
        flex-direction: column;
        align-items: center;
      }
      .container > * {
        margin-top: 10px;
      }
    `;
  }
}

window.customElements.define(UsersAdminPage.is, UsersAdminPage);