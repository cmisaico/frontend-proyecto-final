import { CellsPage } from '@cells/cells-page';
import { CellsI18nMixin as i18n } from '@cells-components/cells-i18n-mixin/cells-i18n-mixin.js';
import { html, css } from 'lit-element';
import { spread } from '@open-wc/lit-helpers';

import '@bbva-web-components/bbva-list-movement';
import '@bbva-web-components/bbva-help-modal';
import '@bbva-web-components/bbva-button-default';
import { getMovements } from '../../elements/movements-dm/movements-dm.js';
import { normalizeUriString } from '../../elements/utils/text.js';

class AccountDetailPage extends i18n(CellsPage) {
  static get is() {
    return 'account-detail-page';
  }

  static get properties() {
    return {
      accountTitle: { type: String },
      movements: { type: Array },
      accountIdUser: { type: String },
      accountId: { type: String },
      accountName: { type: String },
      accountAmount: { type: Number },
      accountNumber: { type: String }
    };
  }

  constructor() {
    super();
    this.movements = [];
  }

  onPageEnter() {
    this.subscribe('account_title', (accountTitle) => this.accountTitle = accountTitle);
    this.subscribe('id_account', (accountId) => this.accountId = accountId);
    this.subscribe('id_user', (idUser) => this.accountIdUser = idUser);
    this.subscribe('name', (name) => this.accountName = name);
    this.subscribe('amount', (amount) => this.accountAmount = Number(amount));
    this.subscribe('number', (number) => this.accountNumber = number);
    
    if (!this.movements.length) {
        getMovements(this.accountId).then((movements) => {
          this.movements = movements;
    });
    }
  }

  onPageLeave() {
    this.movements = [];
  }

  get movementList() {
    if (!this.movements.length) {
      return html`
        <div>
        </div>
        `;
    }

    return this.movements.map((movement) => {
      const movementProperties = this.buildMovementProperties(movement);
      return html`
        <bbva-list-movement
          ...="${spread(movementProperties)}">
        </bbva-list-movement>
      `;
    });
  }

  buildMovementProperties(movement) {
    const { concept, date, amount} = movement;

    return {
      ...(date && { 'description': date }),
      ...(concept && { 'card-title': concept }),
      ...(amount && { 'amount': amount, 'local-currency': 'PEN', 'currency-code': 'PEN' }),
      'class': 'bbva-global-divider',
      'aria-label': 'Ver detalle del pago con tarjeta',
      'language': 'es',
    };
  }

  makeTransference() {
    this.publish('transfer_title', this.accountName);
    this.publish('id_account', this.accountId);
    this.publish('id_user', this.accountIdUser);
    this.publish('name', this.accountName);
    this.publish('amount', this.accountAmount);
    this.publish('number', this.accountNumber);
    this.navigate('transfer');
  }

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__header">
          <bbva-header-main
            icon-left1="coronita:return-12"
            accessibility-text-icon-left1="Volver"
            text=${this.accountTitle}
            @header-icon-left1-click=${() => window.history.back()}>
          </bbva-header-main>
        </div>

        <div slot="app__main">
        <div slot="app__section" class='container'>
            <label class='account-style-text'>${this.accountName}</label>
            <br/>
            <label class='amount-style-text'>S/${this.accountAmount}</label>
            <label class='available-style-text'>Saldo Disponible</label>
            <bbva-button-default @click = ${this.makeTransference} >Transferir</bbva-button-default>
        </div>
        <div slot="app__section" class='movement-group-style'>
          ${this.movementList ? html`${this.movementList}` : html`<cells-skeleton-loading-page visible></cells-skeleton-loading-page>`}
        </div>
          <bbva-help-modal
            id="logoutModal" 
            header-icon="coronita:info" 
            header-text=${this.t('dashboard-page.logout-modal.header')}
            button-text=${this.t('dashboard-page.logout-modal.button')}
            @help-modal-footer-button-click=${() => window.cells.logout()}>
            <div slot="slot-content">
              <span>${this.t('dashboard-page.logout-modal.slot')}</span>
            </div>
          </bbva-help-modal>
        </div>
     </cells-template-paper-drawer-panel>`;
  }

  static get styles() {
    return css`
      bbva-header-main {
        --bbva-header-main-bg-color: #002171;
      }

      cells-template-paper-drawer-panel {
        background-color: #5472d3;
      }
      .container {
        padding-top: 2em;
        display: flex;
        flex-direction: column;
        align-items: center;
      }
      cells-skeleton-loading-page {
          margin-top: 11.5em;
      }
      .account-style-text {
        font-size: 22px;
        font-weight: bold;
      }
      .amount-style-text {
        font-size: 20px;
        font-weight: bold;
      }
      .available-style-text {
        font-size: 14px;
      }
      bbva-button-default {
        margin-top: 1em;
      }
      .movement-group-style {
        padding-top: 1em;
      }
    `;
  }
}

window.customElements.define(AccountDetailPage.is, AccountDetailPage);