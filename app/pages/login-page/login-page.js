import { CellsPage } from '@cells/cells-page';
import { CellsI18nMixin as i18n } from '@cells-components/cells-i18n-mixin/cells-i18n-mixin.js';
import { html, css } from 'lit-element';

import '@bbva-web-components/bbva-form-field';
import '@bbva-web-components/bbva-form-password';
import '@bbva-web-components/bbva-button-default';
import { normalizeUriString } from '../../elements/utils/text.js';
import { login } from '../../elements/login-dm/login-dm';

class LoginPage extends i18n(CellsPage) {
  static get is() {
    return 'login-page';
  }

  static get properties() {
    return {
      error: { type: Boolean },
    };
  }

  
  constructor() {
    super();
    this.error = false;
  }

  firstUpdated(changedProps) {
    super.firstUpdated(changedProps);
    this._dniInput = this.shadowRoot.querySelector('#dni');
    this._passwordInput = this.shadowRoot.querySelector('#password');
  }

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__header">
          <bbva-header-main
            text="BBVA"">
          </bbva-header-main>
        </div>

        <div slot="app__main" class="container">
          <bbva-form-field
            id="dni"
            auto-validate
            validate-on-blur 
            label=${this.t('login-page.user-dni')}
            required>
          </bbva-form-field>

          <bbva-form-password
            id="password"
            auto-validate
            validate-on-blur 
            label=${this.t('login-page.password-input')}
            required>
          </bbva-form-password>

          <bbva-button-default 
            @click=${this.handleValidation}>
              ${this.t('login-page.button')}
          </bbva-button-default>

          <div class='${this.error?"with-error":"without-error"}'>
            Las credenciales son incorrectas
          </div>

        </div>
     </cells-template-paper-drawer-panel>`;
  }

  handleValidation() {
    let canContinue = true;

    [this._dniInput, this._passwordInput].forEach((el) => (el.validate(), el.invalid && (canContinue = false)));

    let body = {
      dni: this._dniInput.value,
      password: this._passwordInput.value
    }

    login(body).then((result)=>{
      canContinue= (result.userid !== undefined)?true:false;
      if (canContinue) {
        this.error = false;
        if(result.role == 'ADMIN') {
          this.publish('user_admin_title', result.name);
          this.navigate('users-admin', {label: normalizeUriString(result.name), id: result.userid });          
        } else {
          this.publish('user_name', result.name);
          this.navigate('dashboard', {label: normalizeUriString(result.name), id: result.userid });          
        }
        this.publish('user_id', result.userid);
      } else {
        this.error = true;
      }
      this.render();
    });


  }

  onPageEnter() {

  }

  onPageLeave() {

  }

  static get styles() {
    return css`

      .with-error {
        margin-top: 2em;
        color: red;
        font-size: 16px;
        display: block;
      }

      .without-error {
        display: none;
      }

      bbva-header-main {
        --bbva-header-main-bg-color: #002171;
      }

      cells-template-paper-drawer-panel {
        background-color: #5472d3;
      }

      .container {
        display: flex;
        flex-direction: column;
        align-items: center;
      }

      .container > * {
        margin-top: 10px;
      }
    `;
  }
}

window.customElements.define(LoginPage.is, LoginPage);