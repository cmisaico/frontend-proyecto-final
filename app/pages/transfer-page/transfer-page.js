import { CellsPage } from '@cells/cells-page';
import { CellsI18nMixin as i18n } from '@cells-components/cells-i18n-mixin/cells-i18n-mixin.js';
import { html, css } from 'lit-element';
import { spread } from '@open-wc/lit-helpers';

import '@bbva-web-components/bbva-list-movement';
import '@bbva-web-components/bbva-help-modal';
import '@bbva-web-components/bbva-button-default';
import { createMovement } from '../../elements/movements-dm/movements-dm.js';
import { updateAccount } from '../../elements/accounts-dm/accounts-dm.js';

class TransferPage extends i18n(CellsPage) {
  static get is() {
    return 'transfer-page';
  }

  static get properties() {
    return {
      transferTitle: { type: String },
      accountIdUser: { type: String },
      accountId: { type: String },
      accountName: { type: String },
      accountAmount: { type: Number },
      accountNumber: { type: String }
    };
  }

  constructor() {
    super();
  }

  onPageEnter() {
    this.subscribe('transfer_title', (transferTitle) => this.transferTitle = transferTitle);
    this.subscribe('id_account', (accountId) => this.accountId = accountId);
    this.subscribe('id_user', (idUser) => this.accountIdUser = idUser);
    this.subscribe('name', (name) => this.accountName = name);
    this.subscribe('amount', (amount) => this.accountAmount = Number(amount));
    this.subscribe('number', (number) => this.accountNumber = number);
    this.subscribe('account_id', (accountId) => this.accountId = accountId);
  }

  transfer(){
    var currentdate = new Date();
    let movement = {
      id_account: this.accountId,
      number: this.shadowRoot.querySelector('#number').value,
      discount: this.shadowRoot.querySelector('#discount').value,
      concept: this.shadowRoot.querySelector('#concept').value,
      date: currentdate.getDay() + "/"+currentdate.getMonth() + "/" + currentdate.getFullYear() + " @ " + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds()
    }
    createMovement(movement).then(
      (result) => {
        let operation = Number(this.accountAmount) - Number(result.amount);
        let body = {
          id_account: this.accountId,
          id_user: this.accountIdUser,
          name: this.accountName,
          amount: operation,
          number: this.accountNumber
        }
        updateAccount(body).then((amount)=>{
          this.publish('amount', operation);
          this.shadowRoot.querySelector('#number').value = '',
          this.shadowRoot.querySelector('#discount').value = '',
          this.shadowRoot.querySelector('#concept').value = '',
          this.render();
        });
      }
    );
  }

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__header">
          <bbva-header-main
            icon-left1="coronita:return-12"
            accessibility-text-icon-left1="Volver"
            text=${this.transferTitle}
            @header-icon-left1-click=${() => window.history.back()}>
          </bbva-header-main>
        </div>

        <div slot="app__main" class="container">
          <bbva-form-field
            id="number"
            auto-validate
            validate-on-blur 
            label=${this.t('transfer-page.account-input')}
            required>
          </bbva-form-field>

          <bbva-form-field
            id="discount"
            auto-validate
            validate-on-blur 
            label=${this.t('transfer-page.amount-input')}
            required>
          </bbva-form-field>

          <bbva-form-field
            id="concept"
            auto-validate
            validate-on-blur 
            label=${this.t('transfer-page.concept-input')}
            required>
          </bbva-form-field>

          <bbva-button-default 
            @click=${this.transfer}>
              ${this.t('transfer-page.button')}
          </bbva-button-default>

          <bbva-help-modal
            id="logoutModal" 
            header-icon="coronita:info" 
            header-text=${this.t('dashboard-page.logout-modal.header')}
            button-text=${this.t('dashboard-page.logout-modal.button')}
            @help-modal-footer-button-click=${() => window.cells.logout()}>
            <div slot="slot-content">
              <span>${this.t('dashboard-page.logout-modal.slot')}</span>
            </div>
          </bbva-help-modal>
        </div>
     </cells-template-paper-drawer-panel>`;
  }

  static get styles() {
    return css`
      bbva-header-main {
        --bbva-header-main-bg-color: #002171;
      }

      cells-template-paper-drawer-panel {
        background-color: #5472d3;
      }
      .container {
        display: flex;
        flex-direction: column;
        align-items: center;
      }
      .container > * {
        margin-top: 10px;
      }
    `;
  }
}

window.customElements.define(TransferPage.is, TransferPage);